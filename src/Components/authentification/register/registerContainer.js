import React, { Component } from 'react' ;
import {connect} from 'react-redux';
import Register from './register';
import {setUsernameText, setPasswordText, 
        setLoginText, setAgeText, 
            setPhonenumberText, register} from '../../../Store/Register/actions';
import { signIn } from '../../../Store/SignIn/actions';

class RegisterContainer extends Component{
    render(){
        return(
            <Register
                username={this.props.username}
                password={this.props.password}
                login={this.props.login}
                age={this.props.age}
                phonenumber={this.props.phonenumber}
                status={this.props.status}
                error={this.props.error}
                setUsernameText={this.props.setUsernameText}
                setPasswordText={this.props.setPasswordText}
                setLoginText={this.props.setLoginText}
                setAgeText={this.props.setAgeText}
                setPhonenumberText={this.props.setPhonenumberText}
                register={this.props.register}
                signIn={this.props.signIn}
            />
        )
    }
}

const mapStateToProps = ( state ) => {
    return {
        username : state.register.username,
        password : state.register.password,
        login : state.register.login,
        age : state.register.age,
        phonenumber : state.register.phonenumber,
        status : state.register.status,
        error : state.register.error
    }
}

const mapDispatchToProps = {
    setUsernameText,
    setPasswordText,
    setLoginText,
    setAgeText,
    setPhonenumberText,
    register,
    signIn
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);