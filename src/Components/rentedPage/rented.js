import React, { Component } from 'react';

class Rented extends Component{ 
    render(){
        return(
            <>
                <h1> Your rented films</h1>
                <div> 
                { this.props.userToken && localStorage.getItem('token') ? 
                    <h1> List of your rented films </h1>
                    : <h1> Sign in to rent films  </h1>
                }

                </div>
            </>
        )    
    }
}

export default Rented;