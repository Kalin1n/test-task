import React, { Component } from 'react';
import {connect} from 'react-redux';
import BasicLayout from '../basicPage'
import Rented from './rented'

class RentedPageContainer extends Component{
    render(){
        return (
            <>
                <BasicLayout> 
                    <Rented 
                        userToken={this.props.userToken}
                    />
                </BasicLayout>
            </>
        )
    }
}

const mapStateToProps = ( state ) => {
    return { 
        userToken : state.signIn.payload
    }
}
export default connect (mapStateToProps)(RentedPageContainer);