import React, { Component } from 'react';
import BasicLayout from '../basicPage'

class Homepage extends Component {
    render(){
        return (
            <BasicLayout> 
                <h1> Film API client </h1>
            </BasicLayout>
        )
    }
}

export default Homepage;